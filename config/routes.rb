Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :routes
  resources :stops do
    collection do
      get 'near_location'
      get 'by_route'
    end
  end
end
