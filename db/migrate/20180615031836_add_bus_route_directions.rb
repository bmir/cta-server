require 'thread'
require 'direction'
class AddBusRouteDirections < ActiveRecord::Migration[5.1]
  include Constants

  THREAD_POOL_SIZE = 5

  def change
    #Route.where(transit_type: TRANSIT_TYPE[:bus]).in_batches do |routes|
    #  routes.each { |route|
    #    route.update(directions:
    #                     Direction.get_directions_from_cta(route.name)
    #                         .join(','))
    #  }
    #  end
    #
    routes = Route.where(transit_type: TRANSIT_TYPE[:bus])
    queue = Queue.new
    routes.each { |route| queue << route }

    directions = {}

    workers = Array.new(THREAD_POOL_SIZE) do
      Thread.new do
        begin
          until queue.empty?
            route = queue.pop(true)
            directions[route.name] = Direction.get_directions_from_cta(route.name).join(',')
          end
        rescue ThreadError => e
          logger.error I18n.t("error_retrieving_bus_route_directions", route_name: route.name, error: e.message)
        end
      end
    end

    workers.map(&:join)

    routes = Route.where(transit_type: TRANSIT_TYPE[:bus])
    queue = Queue.new
    routes.each { |route| queue << route }

    workers = Array.new(THREAD_POOL_SIZE) do
      Thread.new do
        until queue.empty?
          route = queue.pop(true)

          ActiveRecord::Base.connection_pool.with_connection do |connection|
            route.update(directions: directions[route.name])
          end
        end

      end
    end

    workers.map(&:join)

    #routes.each { |route| route.update(directions: directions[:route_name])}

  end
end
