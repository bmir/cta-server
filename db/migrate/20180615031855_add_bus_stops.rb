require 'thread'
require 'stop'

class AddBusStops < ActiveRecord::Migration[5.1]
  include Constants

  THREAD_POOL_SIZE = 5

  def change
    queue = Queue.new
    routes = Route.where(transit_type: TRANSIT_TYPE[:bus])

    routes.each do |route|
      directions = route.directions

      if directions.is_a? Array
        route.directions.split(",").each { |direction|
          queue.push(route: route, direction: direction)
        }
      else
        queue.push(route: route, direction: directions) if directions.present?
      end
    end

    puts "Queue size: #{queue.size}"

    workers = Array.new(THREAD_POOL_SIZE) do
      Thread.new do
        begin
          until queue.empty?
              route_and_direction = queue.pop(true)
              Stop.refresh_stops_for_route_and_direction route_and_direction[:route], route_and_direction[:direction]
          end
        rescue ThreadError
          puts "Error..."
        end
      end
    end

    workers.map(&:join)
  end
end
