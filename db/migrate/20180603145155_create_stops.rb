class CreateStops < ActiveRecord::Migration[5.1]
  def change
    create_table :stops do |t|
      t.string :stop_id
      t.integer :route_id
      t.string :direction
      t.string :name
      t.decimal :latitude, precision: 14, scale: 12
      t.decimal :longitude, precision: 14, scale: 12

      t.timestamps
    end
  end
end
