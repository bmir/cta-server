class AddTrainRoutes < ActiveRecord::Migration[5.1]
  def change
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('red', 1, 'C60C30', 'Red', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('blue', 1, '00A1DE', 'Blue', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('brn', 1, '62361B', 'Brown', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('g', 1, '009B3A', 'Green', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('org', 1, 'F9461C', 'Orange', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('p', 1, '522398', 'Purple', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('pexp', 1, '522398', 'Purple Express', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('pink', 1, 'E27EA6', 'Pink', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
    execute "insert into routes (name, transit_type, color, descriptor, created_at, updated_at) values ('y', 1, 'F9E300', 'Yellow', '#{Time.now.strftime('%F %T')}', '#{Time.now.strftime('%F %T')}')"
  end
end
