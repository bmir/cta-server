class CreateRoutes < ActiveRecord::Migration[5.1]
  def change
    create_table :routes do |t|
      t.string :name
      t.integer :transit_type
      t.string :color
      t.string :descriptor
      t.string :directions

      t.timestamps
    end
  end
end
