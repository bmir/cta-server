module Cta
  module Bus
    module Base
      include Cta::Bus::Constants

      def self.bus_key
        Constants::CTA_BUS_API_KEY
      end

      def self.bus_host
        Constants::CTA_BUS_TRACKER_FULL_URL
      end

      def self.bus_key_param
        CTA_BUS_KEY_PARAM + "=" + bus_key
      end

      def self.bus_response_as_json_param
        BUS_JSON_FORMAT_PARAM
      end

      def self.bus_host_with_uri(uri)
        CTA_BUS_TRACKER_FULL_URL + uri
      end

      def self.construct_bus_params(params)

        params = "&#{params}" unless params.empty?
        "?#{bus_key_param}&#{bus_response_as_json_param}" + params
      end

      def self.make_bus_request(uri, params)
        full_url = bus_host_with_uri(uri) + construct_bus_params(params)
        response = HTTParty.get(full_url)
        response_json = response.as_json
      end

    end # Base
  end
end