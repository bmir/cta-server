require 'httparty'

module Cta
  module Bus
    module Prediction
      include Cta::Bus::Base
      include Cta::Bus::Constants

      def bus_prediction_by_stpid(rt, stpid)
        bus_prediction rt, nil, stpid
      end

      def bus_prediction_by_vid(rt, vid)
        bus_prediction rt, vid, nil
      end

      def bus_prediction(rt, vid, stpid)
        params = bus_params(rt, vid, stpid)
        make_bus_request CTA_BUS_API_PREDICTION_URI, params
      end

      def bus_params(rt, vid, stpid)
        unless rt.blank?
          params = "rt=#{rt}"
          params += "&vid=#{vid}" unless vid.blank?
          params += "&stpid=#{stpid}" if vid.blank? and !stpid.blank?
        end

          params
      end
    end
  end
end