require 'httparty'

module Cta
  module Bus
    module Direction
      include Cta::Bus::Constants
      include Cta::Bus::Base

      def self.get_directions(rt)
        Cta::Bus::Base.make_bus_request CTA_BUS_API_DIRECTION_URI, "rt=#{rt}"
      end
    end
  end
end