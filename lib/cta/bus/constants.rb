module Cta
  module Bus
    module Constants
      CTA_BUS_API_PORT = 80
      CTA_BUS_API_HOST = 'www.ctabustracker.com'.freeze
      CTA_BUS_TRACKER_URI = "http://#{CTA_BUS_API_HOST}/"
      CTA_BUS_TRACKER_CONTEXT = 'bustime/api/'.freeze
      CTA_BUS_TRACKER_VERSION = 'v1/'.freeze
      CTA_BUS_TRACKER_FULL_URL = CTA_BUS_TRACKER_URI + CTA_BUS_TRACKER_CONTEXT + CTA_BUS_TRACKER_VERSION

      # Tracker Key Info
      CTA_BUS_API_KEY = Rails.application.secrets.cta_api_bus_key
      CTA_BUS_KEY_PARAM = 'key'.freeze

      # Other Keys
      BUS_JSON_FORMAT_PARAM = 'format=json'.freeze

      # Various API URIs
      CTA_BUS_API_DIRECTION_URI = 'getdirections'.freeze
      CTA_BUS_API_PATTERN_URI = 'getpatterns'.freeze
      CTA_BUS_API_PREDICTION_URI = 'getpredictions'.freeze
      CTA_BUS_API_ROUTE_URI = 'getroutes'.freeze
      CTA_BUS_API_SERVICE_BULLETIN_URI = 'getservicebulletins'.freeze
      CTA_BUS_API_STOP_URI = 'getstops'.freeze
      CTA_BUS_API_TIME_URI = 'gettime'.freeze
      CTA_BUS_API_VEHICLE_URI = 'getvehicles'.freeze

      # JSON response properties
      CTA_BUS_JSON_ROOT_PROPERTY = 'bustime_response'.freeze
      CTA_BUS_JSON_ROUTE_PROPERTY = 'route'.freeze
      CTA_BUS_JSON_STOP_PROPERTY = 'stop'.freeze
      CTA_BUS_JSON_DIRECTIONS_PROPERTY = 'dir'.freeze
    end
  end
end
