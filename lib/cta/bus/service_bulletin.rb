require 'httparty'

module Cta
  module Bus
    module ServiceBulletin
      include Cta::Bus::Base
      include Cta::Bus::Constants

      def get_service_bulletin(rt, rtdir, stpid)

        make_bus_request CTA_BUS_API_SERVICE_BULLETIN_URI, "stpid=#{stpid}" if !stpid.blank? and rt.blank?
        make_bus_request CTA_BUS_API_SERVICE_BULLETIN_URI, "rt=#{rt}&rtdir=#{rtdir}&stpid=#{stpid}" if !rt.blank? and !rtdir.blank? and !stpid.blank?
        make_bus_request CTA_BUS_API_SERVICE_BULLETIN_URI, "rt=#{rt}&rtdir=#{rtdir}" if !rt.blank? and !rtdir.blank?
        make_bus_request CTA_BUS_API_SERVICE_BULLETIN_URI, "rt=#{rt}" if !rt.blank?

      end
    end
  end
end