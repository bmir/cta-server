require 'httparty'

module Cta
  module Bus
    module Vehicle
      include Cta::Bus::Constants

      def bus_vehicles_by_rt(rt)
        unless rt.blank?
          make_bus_request CTA_BUS_API_VEHICLE_URI, "rt=#{rt}"
        end
      end
    end

    def bus_vehicle_by_vid(vid)
      unless vid.blank?
        make_bus_request CTA_BUS_API_VEHICLE_URI, "vid=#{vid}"
      end
    end
  end
end