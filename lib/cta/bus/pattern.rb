require 'httparty'

module Cta
  module Bus
    module Pattern
      include Cta::Bus::Base
      include Cta::Bus::Constants

      def get_pattern(rt, pid)
        params = get_bus_params(rt, pid)
        make_bus_request CTA_BUS_API_PATTERN_URI, params
      end

      def get_bus_params(rt, pid)
        return "rt=#{rt}" unless rt.nil?
        return "pid=#{pid}" unless pid.nil?
        ""
      end
    end
  end
end