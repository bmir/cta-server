require 'httparty'

module Cta
  module Bus
    module Stop
      include Cta::Bus::Base
      include Cta::Bus::Constants

      def self.bus_stops(rt, dir)
        Cta::Bus::Base.make_bus_request CTA_BUS_API_STOP_URI, "rt=#{rt}&dir=#{dir}"
      end
    end
  end
end