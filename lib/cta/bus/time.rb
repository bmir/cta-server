require 'httparty'

module Cta
  module Bus
    module Time
      include Cta::Bus::Constants
      include Cta::Bus::Base

      def bus_time
        make_bus_request CTA_BUS_API_TIME_URI, ""
      end
    end
  end
end