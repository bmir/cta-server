require 'httparty'


module Cta
  module Bus
    module Route
      include Cta::Bus::Base
      include Cta::Bus::Constants

      def self.bus_routes
        Base.make_bus_request CTA_BUS_API_ROUTE_URI, ""
      end
    end
  end
end