require 'httparty'
require 'json'

module Cta
  module Train
    module Route
      include Cta::Train::Base
      include Cta::Train::Constants::Routes

      def train_routes
        TRAIN_LIST
      end
    end
  end
end