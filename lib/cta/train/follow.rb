require 'httparty'

module Cta
  module Train
    module Arrivals
      include Cta::Train::Constants
      include Cta::Train::Base

      def future_train_position_times(runNumber)
        make_train_request(CTA_TRAIN_FOLLOW_TRAIN, "mapid=40380&max=5")
      end
    end
  end
end