require 'httparty'

module Cta
  module Train
    module Arrivals
      include Cta::Train::Constants
      include Cta::Train::Base

      # @mapid (station)
      # @stpid (part of station -- i.e. platform)
      # @rt (train route)
      # @max
      def get_train_arrivals(mapid, stpid, rt, max)
        make_train_request(CTA_TRAIN_ARRIVALS, "mapid=40380&max=5")
      end
    end
  end
end