require 'httparty'

module Cta
  module Train
    module Time
      include Cta::TRAIN::Constants
      include Cta::Train::Base

      def get_train_time
        make_train_request CTA_TRAIN_API_TIME_URI, ""
      end
    end
  end
end