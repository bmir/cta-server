module Cta
  module Train
    module Base
      include Cta::Train::Constants

      def train_key
        CTA_TRAIN_API_KEY
      end

      def train_host
        CTA_TRAIN_TRACKER_FULL_URL
      end

      def train_key_param
        CTA_TRAIN_KEY_PARAM + "=" + train_key
      end

      def train_host_with_uri(uri)
        CTA_TRAIN_TRACKER_FULL_URL + uri
      end

      def train_response_as_json_param
        TRAIN_JSON_FORMAT_PARAM
      end

      def construct_train_params(params)

        unless params.empty?
          params = "&#{params}"
        end

        "?#{train_key_param}" + params
      end

      def make_train_request(uri, params)
        full_url = train_host_with_uri(uri) + construct_train_params(params)
        response = HTTParty.get(full_url)
        response_json = response.as_json
      end
    end
  end
end