module Cta
  module Train
    module Constants
      CTA_TRAIN_API_PORT = 80
      CTA_TRAIN_API_HOST = 'lapi.transitchicago.com'
      CTA_TRAIN_TRACKER_URI = "http://#{CTA_TRAIN_API_HOST}/"
      CTA_TRAIN_TRACKER_CONTEXT = 'api/'
      CTA_TRAIN_TRACKER_VERSION = '1.0/'
      CTA_TRAIN_TRACKER_FULL_URL = CTA_TRAIN_TRACKER_URI + CTA_TRAIN_TRACKER_CONTEXT + CTA_TRAIN_TRACKER_VERSION

      # Global constants
      TRAIN_JSON_FORMAT_PARAM = 'outputType=JSON'

      # Tracker key info
      CTA_TRAIN_API_KEY = Rails.application.secrets.cta_api_train_key
      CTA_TRAIN_KEY_PARAM = 'key'

      # Various API URIs
      CTA_TRAIN_ARRIVALS = 'ttarrivals.aspx'
      CTA_TRAIN_FOLLOW_TRAIN = 'ttfollow.aspx'
      CTA_TRAIN_TRAIN_POSITION = 'ttpositions.aspx'

      # Follow Train params
      CTA_TRAIN_FOLLOW_TRAIN_RUNNUMBER = 'runnumber' # required

      # Arrivals params
      CTA_TRAIN_TRAIN_ARRIVAL_MAP_ID = 'mapid' # req'd if no stpid specified
      CTA_TRAIN_TRAIN_ARRIVAL_STOP_ID = 'stopid' # req'd if no mapid specified
      CTA_TRAIN_TRAIN_ARRIVAL_MAX_RESULTS = 'max' # optional
      CTA_TRAIN_TRAIN_ARRIVAL_ROUTE = 'rt' # optional

      # Position params
      CTA_TRAIN_POSITION_ROUTE = 'rt' # required, can be multiple values (comma-delimited?)

      module Routes
        RED = 'Red'
        BLUE = 'Blue'
        BROWN = 'Brn'
        GREEN = 'G'
        ORANGE = 'Org'
        PURPLE = 'P'
        PURPLE_EXP = 'Pexp'
        PINK = 'Pink'
        YELLOW = 'Y'

        TRAIN_LIST = [RED, BLUE, BROWN, GREEN, ORANGE, PURPLE, PURPLE_EXP, PINK, YELLOW]

        DIRECTIONS = {
            red: {'1': 'Howard', '5': '95th/Dan Ryan'},
            blue: {'1': 'O''Hare', '5': 'Forest Park'},
            brown: {'1': 'Kimball', '5': 'Loop'},
            green: {'1': 'Harlem/Lake', '5': ['Ashland/63rd', 'Cottage Grove']},
            orange: {'1': 'Loop', '5': 'Midway'},
            purple: {'1': 'Linden', '5': %w{Howard Loop}},
            pink: {'1': 'Loop', '5': '54th/Cermak'},
            yellow: {'1': 'Skokie', '5': 'Howard'}
        }

      end
    end
  end
end
