module Cta
  module Constants
    NO_VALID_RESPONSE_CODE = -1
    DEFAULT_NO_VALID_RESPONSE = { error: "Your request generated no results.", errorCode: NO_VALID_RESPONSE_CODE }
  end

end