module Constants
  TRANSIT_TYPE = {
    train: 1,
    bus: 0
  }.freeze

  ROUTES = {
    root_key: 'routes'
  }.freeze

  STOPS = {
    root_key: 'stops',
    latitude_param: :lat,
    longitude_param: :lon,
    radius_param: :radius
  }.freeze

  DIRECTIONS = {
    root_key: 'directions'
  }.freeze
end
