module Constants
  module RouteDirection
    NORTH = 'Northbound'.freeze
    EAST = 'Eastbound'.freeze
    SOUTH = 'Southbound'.freeze
    WEST = 'Westbound'.freeze

    def self.translate_direction_to_cta_value(direction)
      case direction
      when /n/i
        NORTH
      when /e/i
        EAST
      when /s/i
        SOUTH
      when /w/i
        WEST
      end
    end
  end
end