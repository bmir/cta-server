

class RoutesController < ApplicationController
  include Constants

  def show
    transit_type_code = request[:id]
    transit_type_code = transit_type_code.to_i if transit_type_code.present?

    if TRANSIT_TYPE.values.include?(transit_type_code)
      Route.refresh_bus_information if Route.should_refresh_bus_data(Route.all)
      render json: Route.where(transit_type: transit_type_code)
    else
      # return error response
      render json: 'ERROR!'
    end
  end


end
