class StopsController < ApplicationController
  include Constants

  def by_route
    (route_name, route_direction) = [request[:id], request[:direction]]
    query_params = request.query_parameters

    render json: {stops: []} and return

    # Massage additional param(s) if given
    route_direction = route_direction.upcase if route_direction.present?

    route = Route.where('lower(name) = ?', route_name).first
    render json: I18n.t('no_route_found_with_name', route: route_name) and return if route.nil?

    stops = Stop.where(route_id: route.id)
    render json: I18n.t('no_stops_found_with_route_and_direction', params: query_params) and return if stops.empty?

    render json: stops
  end

  def near_location
    (longitude, latitude, radius) = request[STOPS[:longitude_param]], latitude = request[STOPS[:latitude_param]], radius = request[STOPS[:radius_param]]

    render json: {stops: []} and return if longitude.blank? || latitude.blank?

    # @TODO work on radius and figure out scaling on iOS map -- find a way to get this value programmatically
    radius = 0.25 if radius.blank?

    begin
      render json: Stop.within(radius, origin: [latitude, longitude]) and return
    rescue StandardError => e
      logger.error I18n.t('error_finding_stops_with_coordinates_and_radius', latitude: latitude, longitude: longitude, radius: radius)
      render json: {stops: []} and return
    end
  end
end
