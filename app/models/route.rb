require 'activerecord-import'

class Route < ApplicationRecord
  include Constants
  include Cta::Bus::Constants
  include Cta::Bus::Route
  include Cta::Train::Route

  has_many :stops

  def self.refresh_bus_information
    cta_bus_route_data = Cta::Bus::Route.bus_routes

    # Only busresponse is needed to process, train routes remain constant
    return nil if cta_bus_route_data.empty? ||
                  cta_bus_route_data[CTA_BUS_JSON_ROOT_PROPERTY].empty? ||
                  cta_bus_route_data[CTA_BUS_JSON_ROOT_PROPERTY][CTA_BUS_JSON_ROUTE_PROPERTY].empty?

    new_bus_routes = parse_bus_json(cta_bus_route_data)

    Route.where(transit_type: TRANSIT_TYPE[:bus]).destroy_all
    Route.import new_bus_routes
  end

  def self.parse_bus_json(cta_bus_route_data)
    bus_routes = cta_bus_route_data[CTA_BUS_JSON_ROOT_PROPERTY][CTA_BUS_JSON_ROUTE_PROPERTY]
    new_bus_routes = []

    bus_routes.each do |route|
      route_to_add = Route.new(name: route['rt'],
                               color: route['rtclr'],
                               descriptor: route['rtnm'],
                               transit_type: TRANSIT_TYPE[:bus])
      new_bus_routes << route_to_add
    end

    new_bus_routes
  end

  def stale?
    updated_at < 1.month.ago
  end

  def bus_transit?
    transit_type == TRANSIT_TYPE[:bus]
  end

  def self.should_refresh_bus_data(routes)
    routes = routes.where(transit_type: TRANSIT_TYPE[:bus])
    (routes.empty? || routes.first.stale?) && routes.first.bus_transit?
  end

  def train_color_code(train_name)
    # ["Red","Blue","Brn","G","Org","P","Pexp","Pink","Y"]
    color_code = 'FFFFFF'
    case train_name
    when /red/i
      color_code = 'FF0000'
    when /blue/i
      color_code = '0000FF'
    when /brn/i
      color_code = 'A52A2A'
    when /g/i
      color_code = '00FF00'
    when /org/i
      color_code = 'FFA500'
    when /p|pexp/i
      color_code = '800080'
    when /pink/i
      color_code = 'ff69b4'
    when /y/i
      color_code = 'FFFF00'
    end
  end

  # @param [String] transit_type_name
  def self.transit_type_code(transit_type_name)
    case transit_type_name
    when 'bus'
      TRANSIT_TYPE[:bus]
    when 'train'
      TRANSIT_TYPE[:train]
    end
  end

  def transit_type_full
    case transit_type
    when TRANSIT_TYPE[:bus]
      'bus'
    when TRANSIT_TYPE[:train]
      'train'
    end
  end

end
