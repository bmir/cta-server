class Direction < ApplicationRecord
  include Cta::Bus::Direction
  include Cta::Bus::Constants

  def self.get_directions_from_cta(route_name)
    direction_data = Cta::Bus::Direction.get_directions(route_name)
    route_directions = []

    if direction_data[CTA_BUS_JSON_ROOT_PROPERTY].present? &&
       direction_data[CTA_BUS_JSON_ROOT_PROPERTY][CTA_BUS_JSON_DIRECTIONS_PROPERTY].present?
      directions = direction_data[CTA_BUS_JSON_ROOT_PROPERTY][CTA_BUS_JSON_DIRECTIONS_PROPERTY] || []

      return directions.is_a?(Array) ? directions.sort.map { |dir| dir[0].upcase } : [directions[0].upcase]
    end

    route_directions
  end
end
