class Stop < ApplicationRecord
  include Cta::Bus::Stop
  include Constants::RouteDirection
  include Geokit

  acts_as_mappable lat_column_name: :latitude,
                   lng_column_name: :longitude

  belongs_to :route

  def stale?
    created_at < 2.weeks.ago
  end

  def self.refresh(params)
    return if params.blank?
    (route, route_direction) = params
  end
  # @param [Route] route
  # @param [String] route_direction
  def self.refresh_stops_for_route_and_direction(route, route_direction)
    route_type = route.transit_type_full

    case route_type
    when 'bus'
      handle_bus_stops route, route_direction
    when 'train'
      handle_train_stops
    end
  end

  # @param [Route] route
  # @param [String] route_direction
  def self.handle_bus_stops(route, route_direction)
    translated_route_direction = Constants::RouteDirection.translate_direction_to_cta_value(route_direction)
    cta_bus_stops_data = Cta::Bus::Stop.bus_stops route.name, translated_route_direction
    begin
      bus_stops = parse_bus_stops cta_bus_stops_data, route, route_direction

      ActiveRecord::Base.connection_pool.with_connection do |connection|
        Stop.where(route_id: route.id, direction: route_direction).destroy_all
        Stop.import bus_stops
      end

    rescue NoMethodError => e
      logger.error I18n.t('no_stops_found_with_route_and_direction',
                          route: route.name, direction: route_direction)
    end

  end

  def self.parse_bus_stops(cta_bus_stops_data, route, direction)
    bus_stops = cta_bus_stops_data['bustime_response']['stop']
    logger.debug "Bus stops: #{bus_stops}"
    new_bus_stops = []

    if bus_stops.is_a? Array
      bus_stops.each do |stop|
        begin
          stop_to_add = Stop.new(route_id: route.id, stop_id: stop['stpid'], name: stop['stpnm'], direction: direction, longitude: BigDecimal.new(stop['lon']), latitude: BigDecimal.new(stop['lat']))
        rescue StandardError
          logger.error "Route ID: #{route.id} and bus_json: #{cta_bus_stops_data} could not be added."
        end

        new_bus_stops << stop_to_add
      end
    else
      # Single element, non-array stop
      new_bus_stops << Stop.new(route_id: route.id, stop_id: bus_stops['stpid'], name: bus_stops['stpnm'],
                                direction: direction, longitude: BigDecimal.new(bus_stops['lon']), latitude: BigDecimal.new(bus_stops['lat']))
    end

    new_bus_stops
  end

  def self.parse_train_stops
    puts "Hitting train stops"
  end
end
